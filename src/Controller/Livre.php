<?php
namespace App\Controller;

use App\Entity\Book;
use App\Entity\Categorie;
use App\Entity\Collect;
use App\Entity\Commentaire;
use App\Repository\BookRepository;
use App\Repository\CategorieRepository;
use App\Repository\CollectRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\LivreType;
use App\Form\CatType;
use App\Form\CommentaireType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;


class Livre extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function col(): Response
    {
        // Récupère le dépôt lié à la classe Castle
        $repo = $this->getDoctrine()
            ->getRepository(Collect::class);

        // Exécute une requête SELECT
        $livres = $repo->findAll();
        $titre = "Collection de livre";

        return $this->render('home.html.twig', [
            'livres' => $livres,
            'titre' => $titre
        ]);
    }

    /**
     * @Route("/allLivre_col/{id}", name="allLivre_col", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function livresCol(Collect $col): Response
    {
        $livres = $col->getLivre();

        if (!$livres) {

        }

        return $this->render('allLivre.html.twig', [
            'col' => $col,
            'livres' => $livres
        ]);
    }

    /**
     * @Route("/allLivre", name="allLivre")
     */
    public function livres(): Response
    {
        // Récupère le dépôt lié à la classe Castle
        $repo = $this->getDoctrine()
            ->getRepository(Book::class);

        // Exécute une requête SELECT
        $livres = $repo->findAll();

        return $this->render('allLivre.html.twig', [
            'livres' => $livres
        ]);
    }

    /**
     * @Route("/livre/create", name="livre_create")
     */
    public function create(): Response
    {
        // Récupère un manager qui permet de manipuler une entité
        $entityManager = $this->getDoctrine()->getManager();

        // Crée une entité et lui donne des valeurs
        $livre = new Book();
        $livre->setName('');
        $livre->setDescription('');
        $livre->setPicture('');

        // Indique au manager que l'entité doit être sauvegardée (rendue persistente)
        $entityManager->persist($livre);

        // Demande au manager d'exécuter les requêtes SQL (INSERT INTO ici)
        $entityManager->flush();

        return $this->render('livre.html.twig', [
            'livres' => [],
            'livre' => $livre
        ]);
    }

    /**
     * @Route("/livre/{id}", name="livre_readone", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function livre(int $id): Response
    {
        $repo = $this->getDoctrine()
            ->getRepository(Book::class);


        $livre = $repo->find($id);
        $com = $livre->getCommentaires();

        if (!$livre) {
            return $this->redirectToRoute('livres');
        }

        return $this->render('livre.html.twig', [
            'livre' => $livre,
            'com' => $com
        ]);
    }

    /**
     * @Route("/livre_delete/{id}", name="livre_delete", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function livreDelete(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $livre = $em->getRepository(Book::class)->find($id);
        $em->remove($livre);
        $em->flush();

        return $this->render('livre.html.twig', [
            'livre' => $livre
        ]);
    }

    /**
     * @Route("/categorie", name="categorie")
     */
    public function cat(): Response
    {
        // Récupère le dépôt lié à la classe Castle
        $repo = $this->getDoctrine()
            ->getRepository(Categorie::class);

        // Exécute une requête SELECT
        $cat = $repo->findAll();

        return $this->render('categorie.html.twig', [
            'cat' => $cat
        ]);
    }

    /**
     * @Route("/categorie_delete/{id}", name="categorie_delete", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function categorieDelete(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $cat = $em->getRepository(Categorie::class)->find($id);
        $em->remove($cat);
        $em->flush();

        return $this->render('categorie.html.twig', [
            'cat' => $cat
        ]);
    }

    /**
     * @Route("/allLivre_cat/{id}", name="allLivre_cat", requirements={"id"="\d+"})
     * @param int $id
     * @return Response
     */
    public function livresCat(Categorie $cat): Response
    {
        $livres = $cat->getLivre();

        if (!$cat) {
            return $this->redirectToRoute('categorie');
        }

        return $this->render('allLivre.html.twig', [
            'cat' => $cat,
            'livres' => $livres
        ]);
    }

    /**
     * @Route("/create_livre", name="create_livre", methods={"GET","POST"})
     */
    public function newLivre(Request $request, SluggerInterface $slugger): Response
    {
        $livre = new Book();
        $form = $this->createForm(LivreType::class, $livre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $brochureFile = $form->get('img_book')->getData();

            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                try {
                    $brochureFile->move(
                        $this->getParameter('brochures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }

                $livre->setImgBook($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $cat = $livre->getCategories();
            foreach ($cat as $c) {
                $c->addLivre($livre);
                $entityManager->persist($c);
            }
            $entityManager->persist($livre);
            $entityManager->flush();

            return $this->redirectToRoute('allLivre');
        }

        return $this->render('create.html.twig', [
            'livre' => $livre,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create_cat", name="create_cat", methods={"GET","POST"})
     */
    public function newCat(Request $request): Response
    {
        $categorie = new Categorie();
        $form = $this->createForm(CatType::class, $categorie);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categorie = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorie);
            $entityManager->flush();

            return $this->redirectToRoute('categorie');
        }

        return $this->render('create.html.twig', [
            'form' => $form->createView(),
            'categorie' => $categorie
        ]);
    }

    /**
     * @Route("/create_com", name="create_com", methods={"GET","POST"})
     */
    public function newCom(Request $request): Response
    {
        $com = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $com);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $com = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($com);
            $entityManager->flush();

            return $this->redirectToRoute('allLivre');
        }

        return $this->render('create.html.twig', [
            'form' => $form->createView(),
            'com' => $com
        ]);
    }

    /**
     * @Route("livre_readone/{id}/livre_up", name="livre_up", methods={"GET","POST"})
     */
    public function LivreUp(Request $request, Book $livre, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(LivreType::class, $livre);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $brochureFile = $form->get('img_book')->getData();

            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                try {
                    $brochureFile->move(
                        $this->getParameter('brochures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                }

                $livre->setImgBook($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $categorie = $livre->getCategories();
            foreach ($categorie as $c) {
                $c->addLivre($livre);
                $entityManager->persist($c);
            }
            $entityManager->persist($livre);
            $entityManager->flush();

            return $this->redirectToRoute('allLivre');
        }

        return $this->render('update.html.twig', [
            'livre' => $livre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("categorie/{id}/cat_up", name="cat_up", methods={"GET","POST"})
     */
    public function CatUp(Request $request, Categorie $cat): Response
    {
        $form = $this->createForm(CatType::class, $cat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cat = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cat);
            $entityManager->flush();

            return $this->redirectToRoute('categorie');
        }

        return $this->render('update.html.twig', [
            'cat' => $cat,
            'form' => $form->createView(),
        ]);
    }
}