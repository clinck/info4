<?php
namespace App\Controller;

use App\Entity\Book;
use App\Form\LivreType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StaticPage extends AbstractController
{
    /**
     * @Route("/create", name="create")
     */
    public function new(): Response
    {
        return $this->render('create.html.twig', [

        ]);
    }
}

