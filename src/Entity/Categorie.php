<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_cat;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, inversedBy="categories")
     */
    private $livre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $desc_cat;

    public function __construct()
    {
        $this->livre = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCat(): ?string
    {
        return $this->nom_cat;
    }

    public function setNomCat(string $nom_cat): self
    {
        $this->nom_cat = $nom_cat;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getLivre(): Collection
    {
        return $this->livre;
    }

    public function addLivre(Book $livre): self
    {
        if (!$this->livre->contains($livre)) {
            $this->livre[] = $livre;
        }

        return $this;
    }

    public function removeLivre(Book $livre): self
    {
        $this->livre->removeElement($livre);

        return $this;
    }

    public function getDescCat(): ?string
    {
        return $this->desc_cat;
    }

    public function setDescCat(string $desc_cat): self
    {
        $this->desc_cat = $desc_cat;

        return $this;
    }

    public function getDesCat(): ?string
    {
        return $this->des_cat;
    }

    public function setDesCat(?string $des_cat): self
    {
        $this->des_cat = $des_cat;

        return $this;
    }
}
