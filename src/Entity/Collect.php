<?php

namespace App\Entity;

use App\Repository\CollectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CollectRepository::class)
 */
class Collect
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_col;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img_col;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="collection")
     */
    private $livre;

    public function __construct()
    {
        $this->livre = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCol(): ?string
    {
        return $this->nom_col;
    }

    public function setNomCol(string $nom_col): self
    {
        $this->nom_col = $nom_col;

        return $this;
    }

    public function getImgCol(): ?string
    {
        return $this->img_col;
    }

    public function setImgCol(string $img_col): self
    {
        $this->img_col = $img_col;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getLivre(): Collection
    {
        return $this->livre;
    }

    public function addLivre(Book $livre): self
    {
        if (!$this->livre->contains($livre)) {
            $this->livre[] = $livre;
            $livre->setCollection($this);
        }

        return $this;
    }

    public function removeLivre(Book $livre): self
    {
        if ($this->livre->removeElement($livre)) {
            // set the owning side to null (unless already changed)
            if ($livre->getCollection() === $this) {
                $livre->setCollection(null);
            }
        }

        return $this;
    }
}
