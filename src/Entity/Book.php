<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre_book;

    /**
     * @ORM\Column(type="text")
     */
    private $desc_book;

    /**
     * @ORM\Column(type="float")
     */
    private $prix_book;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $auteur_book;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img_book;

    /**
     * @ORM\ManyToMany(targetEntity=Categorie::class, mappedBy="livre")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="livre")
     */
    private $commentaires;

    /**
     * @ORM\ManyToOne(targetEntity=Collect::class, inversedBy="livre")
     */
    private $collection;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreBook(): ?string
    {
        return $this->titre_book;
    }

    public function setTitreBook(string $titre_book): self
    {
        $this->titre_book = $titre_book;

        return $this;
    }

    public function getDescBook(): ?string
    {
        return $this->desc_book;
    }

    public function setDescBook(string $desc_book): self
    {
        $this->desc_book = $desc_book;

        return $this;
    }

    public function getPrixBook(): ?float
    {
        return $this->prix_book;
    }

    public function setPrixBook(float $prix_book): self
    {
        $this->prix_book = $prix_book;

        return $this;
    }

    public function getAuteurBook(): ?string
    {
        return $this->auteur_book;
    }

    public function setAuteurBook(string $auteur_book): self
    {
        $this->auteur_book = $auteur_book;

        return $this;
    }

    public function getImgBook(): ?string
    {
        return $this->img_book;
    }

    public function setImgBook(string $img_book): self
    {
        $this->img_book = $img_book;

        return $this;
    }

    /**
     * @return Collection|Categorie[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categorie $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addLivre($this);
        }

        return $this;
    }

    public function removeCategory(Categorie $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeLivre($this);
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setLivre($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getLivre() === $this) {
                $commentaire->setLivre(null);
            }
        }

        return $this;
    }

    public function getCollection(): ?Collect
    {
        return $this->collection;
    }

    public function setCollection(?Collect $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

}
