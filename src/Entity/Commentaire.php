<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contenu_com;

    /**
     * @ORM\Column(type="date")
     */
    private $date_com;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_com;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="commentaires")
     */
    private $livre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenuCom(): ?string
    {
        return $this->contenu_com;
    }

    public function setContenuCom(string $contenu_com): self
    {
        $this->contenu_com = $contenu_com;

        return $this;
    }

    public function getDateCom(): ?\DateTimeInterface
    {
        return $this->date_com;
    }

    public function setDateCom(\DateTimeInterface $date_com): self
    {
        $this->date_com = $date_com;

        return $this;
    }

    public function getUserCom(): ?string
    {
        return $this->user_com;
    }

    public function setUserCom(string $user_com): self
    {
        $this->user_com = $user_com;

        return $this;
    }

    public function getLivre(): ?Book
    {
        return $this->livre;
    }

    public function setLivre(?Book $livre): self
    {
        $this->livre = $livre;

        return $this;
    }
}
