<?php


namespace App\Form;

use App\Controller\Livre;
use App\Entity\Book;
use App\Entity\Categorie;
use App\Entity\Collect;
use App\Entity\Commentaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
class CommentaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_com', TextType::class, [
                'label' => 'User : ',
            ])
            ->add('date_com', DateType::class, [
                'label' => 'Date : ',
            ])
            ->add('contenu_com', TextType::class, [
                'label' => 'Contenu : ',
            ])
            ->add('livre', EntityType::class, [
                'class' => Book::class,
                'choice_label' => 'titre_book',
                'label' => 'Titre du livre : '
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Commentaire::class,
        ]);
    }
}
