<?php

namespace App\Form;

use App\Controller\Livre;
use App\Entity\Book;
use App\Entity\Categorie;
use App\Entity\Collect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class LivreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre_book', TextType::class, [
                'label' => 'Titre du livre : ',
            ])
            ->add('desc_book', TextType::class, [
                'label' => 'Description du livre : ',
            ])
            ->add('auteur_book', TextType::class, [
                'label' => 'Auteur du livre : ',
            ])
            ->add('prix_book', TextType::class, [
                'label' => 'Prix du livre : ',
            ])
            ->add('img_book', FileType::class, [
                'label' => 'Image : ',

                'mapped' => false,

                'required' => false,

                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            "image/jpeg", "image/png", "image/gif", "image/jpg"
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])

            ->add('categories', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'nom_cat',
                'expanded' => true,
                'multiple' => true,
                'label' => 'Catégorie : '
            ])
            ->add('collection', EntityType::class, [
                'class' => Collect::class,
                'choice_label' => 'nom_col',
                'label' => 'Collection : '
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
