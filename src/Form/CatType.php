<?php

namespace App\Form;

use App\Controller\Livre;
use App\Entity\Book;
use App\Entity\Categorie;
use App\Entity\Collect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom_cat', TextType::class, [
                'label' => 'Nom de la catégorie : ',
            ])
            ->add('desc_cat', TextType::class, [
                'label' => 'Description de la catégorie : ',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Categorie::class,
        ]);
    }
}
