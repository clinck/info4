<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210311093455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, collection_id INTEGER DEFAULT NULL, titre_book VARCHAR(255) NOT NULL, desc_book CLOB NOT NULL, prix_book DOUBLE PRECISION NOT NULL, auteur_book VARCHAR(255) NOT NULL, img_book VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_CBE5A331514956FD ON book (collection_id)');
        $this->addSql('CREATE TABLE categorie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom_cat VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE categorie_book (categorie_id INTEGER NOT NULL, book_id INTEGER NOT NULL, PRIMARY KEY(categorie_id, book_id))');
        $this->addSql('CREATE INDEX IDX_BCCC2466BCF5E72D ON categorie_book (categorie_id)');
        $this->addSql('CREATE INDEX IDX_BCCC246616A2B381 ON categorie_book (book_id)');
        $this->addSql('CREATE TABLE collect (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom_col VARCHAR(255) NOT NULL, img_col VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, livre_id INTEGER DEFAULT NULL, contenu_com VARCHAR(255) NOT NULL, date_com DATE NOT NULL, user_com VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_67F068BC37D925CB ON commentaire (livre_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE categorie_book');
        $this->addSql('DROP TABLE collect');
        $this->addSql('DROP TABLE commentaire');
    }
}
