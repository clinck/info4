<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210319163347 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_CBE5A331514956FD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, collection_id INTEGER DEFAULT NULL, titre_book VARCHAR(255) NOT NULL COLLATE BINARY, desc_book CLOB NOT NULL COLLATE BINARY, prix_book DOUBLE PRECISION NOT NULL, auteur_book VARCHAR(255) NOT NULL COLLATE BINARY, img_book VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_CBE5A331514956FD FOREIGN KEY (collection_id) REFERENCES collect (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO book (id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book) SELECT id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
        $this->addSql('CREATE INDEX IDX_CBE5A331514956FD ON book (collection_id)');
        $this->addSql('ALTER TABLE categorie ADD COLUMN desc_cat VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_BCCC246616A2B381');
        $this->addSql('DROP INDEX IDX_BCCC2466BCF5E72D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__categorie_book AS SELECT categorie_id, book_id FROM categorie_book');
        $this->addSql('DROP TABLE categorie_book');
        $this->addSql('CREATE TABLE categorie_book (categorie_id INTEGER NOT NULL, book_id INTEGER NOT NULL, PRIMARY KEY(categorie_id, book_id), CONSTRAINT FK_BCCC2466BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_BCCC246616A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO categorie_book (categorie_id, book_id) SELECT categorie_id, book_id FROM __temp__categorie_book');
        $this->addSql('DROP TABLE __temp__categorie_book');
        $this->addSql('CREATE INDEX IDX_BCCC246616A2B381 ON categorie_book (book_id)');
        $this->addSql('CREATE INDEX IDX_BCCC2466BCF5E72D ON categorie_book (categorie_id)');
        $this->addSql('DROP INDEX IDX_67F068BC37D925CB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__commentaire AS SELECT id, livre_id, contenu_com, date_com, user_com FROM commentaire');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, livre_id INTEGER DEFAULT NULL, contenu_com VARCHAR(255) NOT NULL COLLATE BINARY, date_com DATE NOT NULL, user_com VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_67F068BC37D925CB FOREIGN KEY (livre_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO commentaire (id, livre_id, contenu_com, date_com, user_com) SELECT id, livre_id, contenu_com, date_com, user_com FROM __temp__commentaire');
        $this->addSql('DROP TABLE __temp__commentaire');
        $this->addSql('CREATE INDEX IDX_67F068BC37D925CB ON commentaire (livre_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_CBE5A331514956FD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, collection_id INTEGER DEFAULT NULL, titre_book VARCHAR(255) NOT NULL, desc_book CLOB NOT NULL, prix_book DOUBLE PRECISION NOT NULL, auteur_book VARCHAR(255) NOT NULL, img_book VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO book (id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book) SELECT id, collection_id, titre_book, desc_book, prix_book, auteur_book, img_book FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
        $this->addSql('CREATE INDEX IDX_CBE5A331514956FD ON book (collection_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__categorie AS SELECT id, nom_cat FROM categorie');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('CREATE TABLE categorie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom_cat VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO categorie (id, nom_cat) SELECT id, nom_cat FROM __temp__categorie');
        $this->addSql('DROP TABLE __temp__categorie');
        $this->addSql('DROP INDEX IDX_BCCC2466BCF5E72D');
        $this->addSql('DROP INDEX IDX_BCCC246616A2B381');
        $this->addSql('CREATE TEMPORARY TABLE __temp__categorie_book AS SELECT categorie_id, book_id FROM categorie_book');
        $this->addSql('DROP TABLE categorie_book');
        $this->addSql('CREATE TABLE categorie_book (categorie_id INTEGER NOT NULL, book_id INTEGER NOT NULL, PRIMARY KEY(categorie_id, book_id))');
        $this->addSql('INSERT INTO categorie_book (categorie_id, book_id) SELECT categorie_id, book_id FROM __temp__categorie_book');
        $this->addSql('DROP TABLE __temp__categorie_book');
        $this->addSql('CREATE INDEX IDX_BCCC2466BCF5E72D ON categorie_book (categorie_id)');
        $this->addSql('CREATE INDEX IDX_BCCC246616A2B381 ON categorie_book (book_id)');
        $this->addSql('DROP INDEX IDX_67F068BC37D925CB');
        $this->addSql('CREATE TEMPORARY TABLE __temp__commentaire AS SELECT id, livre_id, contenu_com, date_com, user_com FROM commentaire');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('CREATE TABLE commentaire (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, livre_id INTEGER DEFAULT NULL, contenu_com VARCHAR(255) NOT NULL, date_com DATE NOT NULL, user_com VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO commentaire (id, livre_id, contenu_com, date_com, user_com) SELECT id, livre_id, contenu_com, date_com, user_com FROM __temp__commentaire');
        $this->addSql('DROP TABLE __temp__commentaire');
        $this->addSql('CREATE INDEX IDX_67F068BC37D925CB ON commentaire (livre_id)');
    }
}
